# Graphical Models

# Graphical Models (Lab)

## Assignments

The assignments are in the `assignments` folder. Each assignment has its own
folder. The assignments are written in Jupyter notebooks. The notebooks are
written in Python 3.11.
