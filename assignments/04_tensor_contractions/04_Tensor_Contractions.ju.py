# %% [markdown]
"""
# Tensor Contractions
In this exercise, we will inspect the canonical parameterization of a graphical model and calculate the normalization constant to answer inference queries.

Later, we will compare the speed of calculating the normalization constant using different orders of tensor contractions.

In the event of a persistent problem, do not hesitate to contact the course instructors under
- paul.kahlmeyer@uni-jena.de

### Submission

- Deadline of submission:
        22.11.2023 00:00
- Submission on [moodle page](https://moodle.uni-jena.de/course/view.php?id=51072)

### Help
In case you cannot solve a task, you can use the saved values within the `help` directory:
- Load arrays with [Numpy](https://numpy.org/doc/stable/reference/generated/numpy.load.html)
```
np.load('help/array_name.npy')
```
- Load functions with [Dill](https://dill.readthedocs.io/en/latest/dill.html)
```
import dill
with open('help/some_func.pkl', 'rb') as f:
    func = dill.load(f)
```

to continue working on the other tasks.
"""

# %% [markdown]
"""
## Graphical Models
Let $p(x)$ be a multivariate categorical on the sample space $\mathcal{X}$.
In the canonical parameterization we define $p$ to be an exponentiated sum of interaction order parameters:
\begin{align}
p(x) = \exp\left(q(x)\right)\,,
\end{align}
where $q(x)$ is a sum of all possible interaction orders
\begin{align}
q(x) = \sum\limits_{k=1}^n\sum\limits_{i=(i_1,\dots,i_k)}q_i(x_{i_1}, \dots, x_{i_k})\,.
\end{align}
In graphical models, we reduce the number of parameters by setting specific interactions $q_i$ to 0.

This notation is a little confusing, so lets exercise trough a **concrete example**.

Consider a multivariate categorical $p(x_0,x_1,x_2,x_3)$.
Furthermore we restrict ourselves to unary and pairwise interaction orders (interactions of order >2 have been set to 0).

This means, that we have single interaction parameter vectors $q_0, q_1, q_2, q_3$ and parwise interaction parameter matrices $q_{01}, q_{02}, q_{03}, q_{12}, q_{13}, q_{23}$.
The $q_i$ hold the (unary) interaction parameters for $x_i$ and $q_{ij}$ holds the interaction parameters for $x_i$ and $x_j$.

With these parameters, the canonical parameterization from above looks like this:
\begin{align}
q(x = [v_0, v_1, v_2, v_3]^T) &=\sum_{i=0}^3 q_i[v_i] + \sum_{j=0, j > i}^3 q_{ij}[v_i, v_j]\\
&=q_0[v_0] + q_1[v_1] + q_2[v_2] + q_3[v_3]\\
&+q_{01}[v_0, v_1] + q_{02}[v_0, v_2] + q_{03}[v_0, v_3]\\
&+q_{12}[v_1, v_2]+q_{13}[v_1, v_3]\\
&+q_{23}[v_2, v_3]\,.
\end{align}



### Task 1

Load $q_i$ and $q_{ij}$ from the pickeled files `q_i.p` and `q_ij.p` respectively.
How large are the sample spaces for each $x_i$?
"""

# %%
# TODO: load q_i, q_ij, determine size of sample spaces
import dill

with open('q_i.p', 'rb') as q_ip:
    q_i = dill.load(q_ip)
print(len(q_i))
print(len(q_i[0]), len(q_i[1]), len(q_i[2]), len(q_i[3]))


with open('q_ij.p', 'rb') as q_ijp:
    q_ij = dill.load(q_ijp)
print(len(q_ij))
print(len(q_ij[0]), len(q_ij[1]), len(q_ij[2]), len(q_ij[3]))
print(len(q_ij[0][0]), len(q_ij[1][0]), len(q_ij[2][0]), len(q_ij[3][0]))
print(len(q_ij[0][0][0]), len(q_ij[1][2][0]), len(q_ij[2][0][0]), len(q_ij[3][0][0]))

t_i = q_i
for n in range(len(t_i)):
    for i in range(len(t_i[n])):
        t_i[n][i] = math.exp(t_i[n][i])

t_ij = q_ij
for n in range(len(t_ij)):
    for n_n in range(len(t_ij[n])):
        for i in range(len(t_ij[n][n_n])):
            for j in range(len(t_ij[n][n_n][i])):
                t_ij[n][n_n][i][j] = math.exp(t_ij[n][n_n][i][j])



# %% [markdown]
"""
## Normalization Constant

Here we have unnormalized probabilities, so we need to calculate the normalization constant first
\begin{align}
K &= \sum_{x}p(x)\\
&= \sum_{x}\exp\left(q(x)\right)\\
&= \sum_{x}\prod_{i} \exp(q_i[x_i])\prod_{j > i} \exp(q_{ij}[x_i, x_j])\\
&= \sum_{x}\prod_{i} t_i[x_i]\prod_{j > i} t_{ij}[x_i, x_j]\,,
\end{align}
where $t_i = \exp(q_i)$ and $t_{ij} = \exp(q_{ij})$ with the elementwise exponential function.

### Task 2

A straighforward way to calculate this constant is iterating over every $x$ and summing up the $p(x)$.

Calculate $K$ using for loops.
"""

# %%
# calculate normalization constant
import itertools
import math

def norm_const_naive(t_i:list, t_ij:list) -> float:
    '''
    Calculates normalization constant by iterating over each x.

    @Params:
        t_i... unary interaction parameters (exponentiated)
        t_ij... binary interaction parameters (exponentiated)

    @Returns:
        normalization constant
    '''

    x_n_n = len(t_i)
    dim = []
    for x_n_i in range(0, x_n_n):
        dim.append(range(len(t_i[x_n_i])))


    K = 0
    X = itertools.product(*dim)
    for x in X:
        # print(x)
        tmp = 1
        for i in range(x_n_n):
            # print(i)
            x_i = x[i]
            # print(x_i)
            tmp *= q_i[i][x_i]
            # print(tmp)
            for j in range(i+1, x_n_n):
                x_j = x[j]
                tmp *= q_ij[i][j][x_i][x_j]
        K += tmp
    return K


# TODO: calculate normalization constant

K = norm_const_naive(t_i, t_ij)
print(K)
assert K == 159744720.1663634

# %% [markdown]
"""
## Inference Queries

With this normalization constant, we can now actually calculate probabilities and answer inference queries.

### Task 3
Calculate the prior marginal
\begin{align}
p(x_3)\,.
\end{align}
"""

# %%
# TODO: calculate p(x_3)

# %% [markdown]
"""
### Task 4

Calculate the probability
\begin{equation}
p(x_2>20)\,.
\end{equation}
"""

# %%
# TODO: calculate p(x_2 > 20)

# %% [markdown]
"""
## Tensor Contraction
Calculating $K$ by iterating over every $x$ is quite slow.
Lets look at how we can speed up this calculation.

We can rewrite the calculation of $K$ as

\begin{align}
K &= \sum_{x}p(x)\\
&= \sum_{x}\prod_{i} \exp(q_i[x_i])\prod_{j > i} \exp(q_{ij}[x_i, x_j])\\
&= \sum_{x}\prod_{i} t_i[x_i]\prod_{j > i} t_{ij}[x_i, x_j]\\
&= \sum_{v_0=1}^{n_0}\sum_{v_1=1}^{n_1}\sum_{v_2=1}^{n_2}\sum_{v_3=1}^{n_3}\prod_{i} t_i[v_i]\prod_{j > i} t_{ij}[v_i, v_j]\,.
\end{align}

In this form, calculating the normalization constant boils down to a single tensor contraction.

Since contracting tensors in numpy is implemented in C under the hood, we can expect a significant speedup.

### Task 5
Calculate the normalization constant using a **single** contraction using the [Einstein-Summation](https://numpy.org/doc/stable/reference/generated/numpy.einsum.html).

For a brief introduction into `einsum`, see [here](https://ajcr.net/Basic-guide-to-einsum/) and [here](https://medium.com/ibm-data-ai/einsum-an-easy-intuitive-way-to-write-tensor-operation-9e12b8a80570).

Make sure that you result is correct by comparing the result to the naive implementation.
"""

# %%
# TODO: use einsum to calculate K

# %% [markdown]
"""
### Task 6

Compare the execution times of calculating $K$ the naive way vs. using `einsum`.
"""

# %%
# TODO: compare execution times

# %% [markdown]
"""
## Contraction order

We see that using contraction speeds up the calculation. This however is not the end of optimization:\
The order of contraction can be permutated, potentially reducing the number of calculations. Here we want to permutate the order in which the variables are marginalized out.

For example for two variables $x_0, x_1$:
\begin{align}
K &= \sum_{v_0=1}^{n_0}\sum_{v_1=1}^{n_1} t_0[v_0]t_1[v_1]t_{01}[v_0, v_1]\\
(1) &= \sum_{v_0=1}^{n_0}t_0[v_0]\sum_{v_1=1}^{n_1}t_1[v_1]t_{01}[v_0, v_1]\\
(2) &= \sum_{v_1=1}^{n_1}t_1[v_1]\sum_{v_0=1}^{n_0}t_0[v_1]t_{01}[v_0, v_1]\\
\end{align}

Can be calculated as (1)
1. Contracting $t_{01}$ and $t_{1}$ over the index $x_1$
2. Contracting the result from 1. with $t_0$ over the index $x_0$

or (2)
1. Contracting $t_{01}$ and $t_{0}$ over the index of $x_0$
2. Contracting the result from 1. with $t_1$ over the index of $x_1$

Depending on the tensor dimensions, one calculation can be faster than the other.


### Task 7

Implement the following function that contracts the tensors in a given order.

As an example for three variables, the order

```
['i', 'j', 'k']
```

with the tensor dictionary

```
tensor_dict = {
'i' : t_i,
'j' : t_j,
'k' : t_k,
'ij' : t_ij,
'ik' : t_ik,
'jk' : t_jk
}
```
will perform the following contractions

1. `tmp = np.einsum('i, ij, ik -> jk', t_i, t_ij, t_ik) # marginalize out i`
2. `tmp = np.einsum('j, jk, jk -> k', t_j, t_jk, tmp) # marginalize out j`
3. `tmp = np.einsum('k, k -> ', t_k, tmp) # marginalize out k`

Make sure that the results are correct and compare the times of different marginalization orders to those from Task 6.
"""

# %%
def norm_const_order(order:list, tensor_dict:dict) -> float:
    '''
    Calculates the normalization constant using tensor contraction with a specific order.

    @Params:
        order... list of variables in the order of their marginalization
        tensor_dict... dict that stores which tensors are for which variable combination

    @Returns:
        normalization constant K

    '''
    # TODO: implement
    pass

# TODO: timing and correctness

# %% [markdown]
"""
## Optimal contraction order

We see that the contraction order has quite a lot of effect on the computation times.

In fact, the problem of finding the best contraction order is generally NP-hard and an active area of research.
In Python, the package [opt_einsum](https://optimized-einsum.readthedocs.io/en/stable/) provides heuristics to find an (near-)optimal contraction order.
"""

# %% [markdown]
"""
### Task 8

Use `opt_einsum` to calculate $K$, make sure result is correct.
Again measure the execution time and compare to the other methods.

Note: if you are interested, you can use `opt_einsum.contract_path` to have a look at the optimal contraction order that was used.
"""

# %%
# TODO: use opt_einsum to compute K

# TODO: timing

